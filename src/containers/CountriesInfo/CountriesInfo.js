import React, {Component, Fragment} from 'react';
import './CountriesInfo.css';
import axios from 'axios';
import Details from '../../components/Details/Details';

class CountriesInfo extends Component {
    state = {
        countries: [],
        countryDetails: null
    };

    componentDidMount() {
        this._makeRequest('/all?fields=name;alpha3Code').then(response => {

            this.setState({countries: response.data})

        }).catch(error => {
            console.log(error);
        });
    }

    _makeRequest = (url) => {
        return axios.get(url).then(response => {
            if (response.status === 200) {
                return response;
            }

            throw new Error('Something wrong');
        });
    };

    countrySelectedHandler = (id) => {
        if (id) {
            axios.get('/alpha/' + id).then(response => {
              this.setState({
                  countryDetails: response.data
              })
            })
        }
    };

  render() {
    return (
            <Fragment>
                <div className="container">
                    <div className="row">
                        <div className="col-md-3 list-block">
                            <div className="list-group">
                              {this.state.countries.map(country => {
                                return <button type="button"
                                               className="list-group-item list-group-item-action"
                                               key={country.alpha3Code}
                                               onClick={() => this.countrySelectedHandler(country.alpha3Code)}
                                >
                                  {country.name}
                                </button>
                              })}
                            </div>
                        </div>
                        <div className="col-md-9 details-block">
                            {this.state.countryDetails !== null &&
                                <Details details={this.state.countryDetails} countries={this.state.countries} />
                            }
                            {this.state.countryDetails === null &&
                                <div className="no-details"><h2>Please select country</h2></div>
                            }
                        </div>
                    </div>
                </div>
            </Fragment>

        );
    }
}

export default CountriesInfo;

