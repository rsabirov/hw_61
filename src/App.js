import React, {Component} from 'react';
import CountriesInfo from "./containers/CountriesInfo/CountriesInfo";

class App extends Component {

  render() {
    return (
      <CountriesInfo />
    )
  }
}

export default App;
