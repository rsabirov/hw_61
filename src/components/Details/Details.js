import React, {Component} from 'react';
import './details.css';

class Details extends Component {
  render() {
      const {countries, details} = this.props;
      const borders = details.borders;

     return (
         <div className="details">
           <div className="row">
             <div className="col-md-8">
               <h1 className="details__title">{this.props.details.name}</h1>
               <ul className="details__list">
                 <li className="details__item"><strong>Capital:</strong><p>{this.props.details.capital}</p></li>
                 <li className="details__item"><strong>Population:</strong><p>{this.props.details.population}</p></li>
                 <li className="details__item"><strong>Region:</strong><p>{this.props.details.region}</p></li>
                 <li className="details__item"><strong>Subregion:</strong><p>{this.props.details.subregion}</p></li>
               </ul>
               <div className="borders">
                 <h5 className="borders__title">Borders with:</h5>
                 <ul className="borders__list">
                     {borders.map((border, i) => {
                         return <li className="borders__list-item" key={i} >{countries.find(c => c.alpha3Code === border).name}</li>;
                     })}
                 </ul>
               </div>
             </div>
             <div className="col-md-4">
               <img src={this.props.details.flag} className="img-thumbnail" alt="flag"/>
             </div>
           </div>
         </div>
     )

  }
}

export default Details;